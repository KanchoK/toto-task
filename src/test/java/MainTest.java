import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

class MainTest {

    private static final String TEST_SORT_DATA ="1 2 3 \n" +
            "1 \n" +
            "3 1";

    private static final String TEST_DATA ="1 2 3 4 5 6 \n" +
            "1 2 3 4 5 7 \n" +
            "1 2 3 4 8 9 \n" +
            "1 2 3 5 9 10 \n" +
            "1 2 4 5 8 10 \n" +
            "1 3 7 8 9 10";

    private static final List<String> TEST_DATA_TOP_5 = Arrays.asList("1", "2", "3", "4", "5");
    private static final List<String> TEST_DATA_BOTTOM_5 = Arrays.asList("6", "7", "8", "9", "10");

    @Test
    void testParseAndSort() {
        List<Map.Entry<String, Integer>> entries = Main.parseAndSortData(TEST_SORT_DATA);

        Assertions.assertEquals("1", entries.get(0).getKey());
        Assertions.assertEquals(3, entries.get(0).getValue());
        Assertions.assertEquals("3", entries.get(1).getKey());
        Assertions.assertEquals(2, entries.get(1).getValue());
        Assertions.assertEquals("2", entries.get(2).getKey());
        Assertions.assertEquals(1, entries.get(2).getValue());
    }

    @Test
    void testTop5() {
        List<Map.Entry<String, Integer>> top5 = Main.getTop5(Main.parseAndSortData(TEST_DATA));

        Assertions.assertEquals(5, top5.size());
        for (int i = 0; i < 5; i++) {
            Assertions.assertTrue(TEST_DATA_TOP_5.contains(top5.get(i).getKey()));
        }
    }

    @Test
    void testBottom5() {
        List<Map.Entry<String, Integer>> bottom5 = Main.getBottom5(Main.parseAndSortData(TEST_DATA));

        Assertions.assertEquals(5, bottom5.size());
        for (int i = 0; i < 5; i++) {
            Assertions.assertTrue(TEST_DATA_BOTTOM_5.contains(bottom5.get(i).getKey()));
        }
    }
}
