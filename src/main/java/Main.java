import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Main {

    private static final String DATA = "8 18 27 45 47 49 \n" +
            "4 10 13 23 43 46 \n" +
            "8 13 28 34 36 46 \n" +
            "21 24 31 36 38 39 \n" +
            "2 8 15 18 25 43 \n" +
            "4 6 10 16 30 42 \n" +
            "8 20 21 36 44 48 \n" +
            "3 16 26 32 38 43 \n" +
            "10 15 23 32 36 43 \n" +
            "5 9 16 26 28 47 \n" +
            "10 14 36 41 44 49 \n" +
            "11 13 15 32 43 46 \n" +
            "7 15 16 25 44 49 \n" +
            "1 12 21 25 28 46 \n" +
            "1 2 7 13 30 38 \n" +
            "11 33 37 44 45 48 \n" +
            "4 7 9 10 21 40 \n" +
            "2 8 16 23 24 39 \n" +
            "4 17 32 37 40 48 \n" +
            "6 16 26 41 44 46 \n" +
            "3 21 22 29 42 46 \n" +
            "3 6 31 32 38 47 \n" +
            "10 19 22 33 39 45 \n" +
            "6 8 21 22 32 37 \n" +
            "10 23 27 31 33 45 \n" +
            "23 24 27 38 39 44 \n" +
            "1 3 13 18 27 39 \n" +
            "7 20 28 38 48 49 \n" +
            "4 22 25 27 28 32 \n" +
            "4 6 14 28 45 48 \n" +
            "1 6 9 10 22 44 \n" +
            "1 12 14 16 17 34 \n" +
            "2 23 31 42 46 49 \n" +
            "16 20 25 32 47 49 \n" +
            "27 29 35 36 41 42 \n" +
            "9 14 20 22 37 49 \n" +
            "3 6 10 20 24 48 \n" +
            "15 18 23 30 42 45 \n" +
            "4 6 19 20 21 39 \n" +
            "5 34 42 43 44 47 \n" +
            "3 14 17 26 41 42 \n" +
            "3 7 20 37 40 41 \n" +
            "11 12 21 37 47 48 \n" +
            "1 23 33 35 37 47 \n" +
            "3 14 21 25 39 42 \n" +
            "4 10 11 12 13 15 \n" +
            "11 15 18 24 26 43 \n" +
            "3 19 33 42 46 48 \n" +
            "8 22 36 37 46 48 \n" +
            "9 10 23 33 39 46 \n" +
            "1 9 18 22 35 46 \n" +
            "6 7 8 12 26 27 \n" +
            "26 28 40 42 46 49 \n" +
            "2 9 11 17 38 46 \n" +
            "5 10 33 40 41 49 \n" +
            "3 10 13 25 32 34 \n" +
            "13 17 22 25 38 39 \n" +
            "4 6 9 14 24 28 \n" +
            "6 20 22 30 42 48 \n" +
            "1 3 10 11 36 43 \n" +
            "11 12 18 32 36 49 \n" +
            "1 8 20 28 37 42 \n" +
            "3 8 11 17 45 49 \n" +
            "8 11 23 27 44 45 \n" +
            "23 33 37 44 46 49 \n" +
            "2 16 17 32 38 48 \n" +
            "6 12 33 43 47 48 \n" +
            "5 10 24 27 39 40 \n" +
            "4 5 35 44 47 49 \n" +
            "8 11 17 26 36 40 \n" +
            "2 8 21 29 32 49 \n" +
            "2 30 32 42 45 46 \n" +
            "7 20 21 39 42 44 \n" +
            "8 14 22 23 38 45 \n" +
            "14 19 20 22 27 36 \n" +
            "3 6 33 34 36 38 \n" +
            "3 23 27 29 31 45 \n" +
            "12 14 23 24 36 45 \n" +
            "3 13 25 29 32 34 \n" +
            "10 14 29 39 43 46 \n" +
            "8 19 21 22 29 44 \n" +
            "6 7 17 38 39 46 \n" +
            "1 13 14 38 48 49 \n" +
            "1 2 8 15 23 40 \n" +
            "1 5 14 31 34 49 \n" +
            "10 18 21 35 42 47 \n" +
            "5 16 24 27 28 34 \n" +
            "17 18 34 38 47 49 \n" +
            "11 31 32 34 40 41 \n" +
            "2 8 19 35 42 44 \n" +
            "9 16 31 34 43 44 \n" +
            "20 21 26 32 44 45 \n" +
            "5 10 21 26 44 46 \n" +
            "9 10 12 23 25 46 \n" +
            "2 3 14 23 27 44 \n" +
            "26 27 29 34 39 48 \n" +
            "7 8 25 28 44 48 \n" +
            "6 8 13 26 27 36 \n" +
            "6 8 9 21 33 45 \n" +
            "5 9 23 27 42 45 \n" +
            "13 18 27 31 41 46 \n" +
            "11 14 20 33 36 42 \n" +
            "14 20 32 46 47 48 \n" +
            "4 11 17 23 25 49 \n" +
            "13 23 26 27 37 47";

    public static void main(String[] args) {
        List<Map.Entry<String, Integer>> entries = parseAndSortData(DATA);
        getTop5(entries);
        getBottom5(entries);
    }

    public static List<Map.Entry<String, Integer>> parseAndSortData(String data) {
        HashMap<String, Integer> numberCounts = new HashMap<>();

        String[] split = data.split("\\n");
        for (int i = 0; i < split.length; i++) {
            List<String> strings = Arrays.asList(split[i].split(" "));
            for (String string : strings) {
                if (numberCounts.containsKey(string)) {
                    Integer count = numberCounts.get(string) + 1;
                    numberCounts.put(string, count);
                } else {
                    numberCounts.put(string, 1);
                }
            }
        }

        return sortByValue(numberCounts);
    }

    public static List<Map.Entry<String, Integer>> getBottom5(List<Map.Entry<String, Integer>> entries) {
        List<Map.Entry<String, Integer>> bottom5 = new ArrayList<>();
        int lastIndex = entries.size() - 1;

        System.out.println("Bottom 5 numbers:");

        for (int i = lastIndex; i > lastIndex - 5; i--) {
            System.out.println("Number: " + entries.get(i).getKey() + " Count: " + entries.get(i).getValue());
            bottom5.add(entries.get(i));
        }

        return bottom5;
    }

    public static List<Map.Entry<String, Integer>> getTop5(List<Map.Entry<String, Integer>> entries) {
        List<Map.Entry<String, Integer>> top5 = new ArrayList<>();

        System.out.println("Top 5 numbers:");

        for (int i = 0; i < 5; i++) {
            System.out.println("Number: " + entries.get(i).getKey() + " Count: " + entries.get(i).getValue());
            top5.add(entries.get(i));
        }

        return top5;
    }

    // function to sort hashmap by values
    public static List<Map.Entry<String, Integer>> sortByValue(HashMap<String, Integer> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list =
                new LinkedList<>(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        return list;
    }
}
